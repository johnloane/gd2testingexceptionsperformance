package dkit.gd2.testingJunit;

public class StringHelper {

    public String truncateAInFirst2Positions(String input){
        if(input.length() <= 2){
            return input.replaceAll("A", "");
        }

        String first2Chars = input.substring(0, 2);
        String stringMinusFirst2Chars = input.substring(2);

        return first2Chars.replaceAll("A", "") + stringMinusFirst2Chars;
    }

    public boolean areFirstTwoAndLastTwoCharactersTheSame(String input){
        if(input.length() <=1){
            return false;
        }
        if(input.length() == 2){
            return true;
        }

        String first2Chars = input.substring(0,2);
        String last2Chars = input.substring(input.length()-2);

        return first2Chars.equals(last2Chars);
    }
}
