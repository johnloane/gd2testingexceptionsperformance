package dkit.gd2.testingJunit;

import org.junit.Before;

import static org.junit.Assert.*;

public class StringHelperTest {

    StringHelper test;

    @Before
    public void before(){
        test = new StringHelper();
    }

    @org.junit.Test
    public void testTruncateAInFirst2Positions_DoubleATest() {
        String expected = "CD";
        String actual = test.truncateAInFirst2Positions("AACD");
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testTruncateAInFirst2Positions_SingleATest() {
        String expected = "CD";
        String actual = test.truncateAInFirst2Positions("ACD");
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testAreFirstTwoAndLastTwoCharactersTheSame() {
        fail("The second test failed");
    }
}